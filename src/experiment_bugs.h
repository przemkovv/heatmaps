
#include "types.h"

ExperimentSettings prepare_experiment_settings_30st() ;

ExperimentSettings prepare_experiment_settings_35st() ;
ExperimentSettings prepare_experiment_settings_40st() ;
ExperimentSettings prepare_experiment_settings_45st() ;

ExperimentSettings prepare_experiment_settings_50st() ;


void experiment_bugs(const ExperimentSettings &settings) ;
