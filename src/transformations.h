
#pragma once

#include "types.h"


cv::Mat get_mask(cv::Mat& image, bool invert = false) ;
std::pair<int, int> find_range(cv::Mat &matrix);

cv::Mat normalize(cv::Mat& image, cv::Mat& mask) ;
Heatmap normalize(Heatmap& heatmap);
cv::Mat transform(Region src_points, Region dst_points, cv::Mat& image, cv::Size dst_size) ;
Heatmap transform(Region src_points, Region dst_points, Heatmap& heatmap, cv::Size dst_size) ;

cv::Mat apply_colormap(cv::Mat image, cv::Mat& colormap) ;
Heatmap apply_colormap(Heatmap& heatmap, cv::Mat& colormap) ;

cv::Mat blend_images(cv::Mat& image1, cv::Mat& image2, cv::Mat& mask) ;
cv::Mat blend_images(cv::Mat& image1, Heatmap& heatmap) ;

cv::Mat merge_horizontally(cv::Mat& image1, cv::Mat& image2, int space = 1) ;
cv::Mat merge_vertically(cv::Mat& image1, cv::Mat& image2, int space = 1) ;

cv::Mat highlight_region(cv::Mat& image, const Region &region) ;
cv::Mat highlight_region(cv::Mat& image, const cv::Rect& region);
