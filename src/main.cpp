
#include <iostream>
#include <string>
#include <utility>
#include <fstream>
#include <string>

#include "types.h"
#include "io.h"
#include "transformations.h"
#include "legend.h"
#include "experiment_gp.h"
#include "experiment_bugs.h"
/* #include "gnuplot-iostream.h" */


extern const int legend_size = 60;

const std::string colormap_files[] = { 
                                      "../data/colormaps/1.jpg",
                                      "../data/colormaps/2.jpg",
                                      "../data/colormaps/3.jpg"
                                     };

void experiment1() { 

    const auto output_dir = "experiment1";
    const bool normalize_input_data = false;

    auto save = [&output_dir](auto filename, auto image) { save_image(filename, image, output_dir); };


    const auto side_view_background_imagename = "../data/2/side_view_background.png";
    const auto top_view_background_imagename = "../data/2/top_background.png";

    const auto heatmap_datafile = "../data/HeatMap_test.csv";

    const Region source_points = { 
        {1, 164},
        {720, 164},
        {720, 576},
        {1, 576}
    };

    const Region dest_points = { 
        {739, 1},
        {739, 668},
        {1, 468},
        {1, 163}
    };

    auto side_view_image = load_image(side_view_background_imagename);
    auto top_view_image = load_image(top_view_background_imagename);
    auto heatmap = read_heatmap_from_csv(heatmap_datafile, normalize_input_data);

    auto side_view_image_w_region = highlight_region(side_view_image, source_points);
    auto top_view_image_w_region = highlight_region(top_view_image, dest_points);

    save("side_view", side_view_image);
    save("top_view", top_view_image);

    view("side_view_image", side_view_image_w_region);
    save("side_view_region", side_view_image_w_region);

    view("top_view_image", top_view_image_w_region);
    save("top_view_region", top_view_image_w_region);

    auto transformed_heatmap = transform(source_points, dest_points, 
                                 heatmap, top_view_image.size());

    

    auto colormap = load_colormap(colormap_files[2]);
    heatmap.image = apply_colormap(heatmap.image, colormap);

    view("heatmap", heatmap.image);
    save("side_view_heatmap", heatmap.image);

    transformed_heatmap.image = apply_colormap(transformed_heatmap.image, colormap);

    view("transformed heatmap", transformed_heatmap.image);
    save("top_view_heatmap", transformed_heatmap.image);

    auto blended_heatmap = blend_images(top_view_image, transformed_heatmap.image, transformed_heatmap.mask); 
    auto with_legend = add_legend_at_left(blended_heatmap, colormap, legend_size, std::make_pair(0,0));

    view("v", with_legend);
    save("blended_heatmap_image", blended_heatmap);
    save("blended_heatmap_image_legend", with_legend);


}

void experiment_punkty_szczegolne() { 

    const auto output_dir = "experiment_punkty_szczegolne";
    const bool normalize_input_data = false;

    auto save = [&output_dir](auto filename, auto image) { save_image(filename, image, output_dir); };


    const auto side_view_background_imagename = "../data/punkty_szczegolne/side_view_daily.png";
    const auto top_view_background_imagename = "../data/punkty_szczegolne/top_view_daily.png";

    const auto heatmap_imagename = "../data/punkty_szczegolne/side_view_daily_heatmap.png";

    const Region source_points = { 
        {1, 164},
        {720, 164},
        {720, 576},
        {1, 576}
    };

    const Region dest_points = { 
        {739, 1},
        {739, 668},
        {1, 468},
        {1, 163}
    };

    auto side_view_image = load_image(side_view_background_imagename);
    auto top_view_image = load_image(top_view_background_imagename);
    auto heatmap = load_image(heatmap_imagename);

    auto side_view_image_w_region = highlight_region(side_view_image, source_points);
    auto top_view_image_w_region = highlight_region(top_view_image, dest_points);

    save("side_view", side_view_image);
    save("top_view", top_view_image);

    view("side_view_image", side_view_image_w_region);
    save("side_view_region", side_view_image_w_region);

    view("top_view_image", top_view_image_w_region);
    save("top_view_region", top_view_image_w_region);

    auto transformed_heatmap = transform(source_points, dest_points, 
                                 heatmap, top_view_image.size());

    cv::Mat heatmap_gray;
    cv::cvtColor(heatmap, heatmap_gray, CV_BGR2GRAY);
    auto heatmap_mask = get_mask(heatmap_gray);

    cv::Mat transformed_heatmap_gray;
    cv::cvtColor(transformed_heatmap, transformed_heatmap_gray, CV_BGR2GRAY);
    auto transformed_heatmap_mask = get_mask(transformed_heatmap_gray);
    

    auto colormap = load_colormap(colormap_files[2]);

    view("heatmap", heatmap);
    save("side_view_heatmap", heatmap);

    view("transformed heatmap", transformed_heatmap);
    save("top_view_heatmap", transformed_heatmap);

    auto blended_heatmap = blend_images(top_view_image, transformed_heatmap, transformed_heatmap_mask); 
    auto blended_heatmap_w_region = highlight_region(blended_heatmap, dest_points);
    auto with_legend = add_legend_at_left(blended_heatmap_w_region, colormap, legend_size, std::make_pair(1,871));

    view("v", with_legend);
    save("blended_heatmap_image", blended_heatmap_w_region);
    save("blended_heatmap_image_legend", with_legend);

    auto blended_heatmap_side = blend_images(side_view_image, heatmap, heatmap_mask); 
    auto blended_heatmap_side_w_region = highlight_region(blended_heatmap_side, source_points);
    auto with_legend_side = add_legend_at_left(blended_heatmap_side_w_region, colormap, legend_size, std::make_pair(1,871));
    view("v_side", with_legend_side);
    save("blended_heatmap_side_image", blended_heatmap_side_w_region);
    save("blended_heatmap_side_image_legend", with_legend_side);


}








void experiment_test_linii() { 

    const auto output_dir = "experiment_test_linii";
    const bool normalize_input_data = false;

    auto save = [&output_dir](auto filename, auto image) { save_image(filename, image, output_dir); };


    const auto side_view_background_imagename = "../data/test_linii/cam_side_view19_background.png";
    const auto top_view_background_imagename = "../data/test_linii/cam_top_view19_background.png";

    const auto lines_imagename = "../data/test_linii/cam_side_view19_linie_do_przetworzenia_black.png";

    const Region source_points = { 
        {123, 44},
        {504, 41},
        {650, 334},
        {-15, 340}
    };

    const Region dest_points = { 
        {509, 459},
        {105, 461},
        {109, 62},
        {496, 61}
    };

    auto side_view_image = load_image(side_view_background_imagename);
    auto top_view_image = load_image(top_view_background_imagename);
    auto lines_image = load_image(lines_imagename);

    auto side_view_image_w_region = highlight_region(side_view_image, source_points);
    auto top_view_image_w_region = highlight_region(top_view_image, dest_points);

    save("side_view", side_view_image);
    save("top_view", top_view_image);

    view("side_view_image", side_view_image_w_region);
    save("side_view_region", side_view_image_w_region);

    view("top_view_image", top_view_image_w_region);
    save("top_view_region", top_view_image_w_region);

    auto transformed_lines = transform(source_points, dest_points, 
                                 lines_image, top_view_image.size());
    cv::Mat transformed_lines_gray;
    cv::cvtColor(transformed_lines, transformed_lines_gray, CV_BGR2GRAY);
    auto transformed_lines_mask = get_mask(transformed_lines_gray, false);
    
    

    auto blended_lines = blend_images(top_view_image, transformed_lines, transformed_lines_mask); 
    auto blended_lines_w_region = highlight_region(blended_lines, dest_points);

    view("v", blended_lines_w_region);
    save("blended_lines_image", blended_lines_w_region);


}




int main() {
    
   /* experiment1(); */ 

    std::vector<ExperimentSettings> settings {
            prepare_experiment_settings_30st(),
            prepare_experiment_settings_35st(),
            prepare_experiment_settings_40st(),
            prepare_experiment_settings_45st(),
            prepare_experiment_settings_50st(),
    };
    std::vector<ExperimentSettings> settings_GP {
        prepare_experiment_settings_parking("06"),
        prepare_experiment_settings_parking("08"),
        prepare_experiment_settings_parking("10"),
        prepare_experiment_settings_parking("12"),
        prepare_experiment_settings_parking("14"),
        prepare_experiment_settings_parking("16"),
        prepare_experiment_settings_parking("18"),
        prepare_experiment_settings_parking("20"),
        prepare_experiment_settings_parking()
    };

    /* demo_png(); */
    std::for_each(settings.begin(), settings.end(), experiment_bugs);
    std::for_each(settings_GP.begin(), settings_GP.end(), experiment_GP);

    /* experiment_test_linii(); */
    /* experiment_punkty_szczegolne(); */


    std::cin.ignore();

    return 0;

}
