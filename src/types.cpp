
#include "types.h"

std::ostream& operator<<(std::ostream& out, const Heatmap& heatmap) {
    out << "Image: " << std::endl;
    out << heatmap.image << std::endl;
    out << "Mask: " << std::endl;
    out << heatmap.mask << std::endl;
    return out;
}

std::ostream& operator<<(std::ostream& out, Statistics stats) {
    out << "Mean: " << stats.mean;
    out << " Sum: " << stats.sum;
    out << " Min: " << stats.min;
    out << " Max: " << stats.max;
    out << " Stddev: " << stats.stddev;
    out << " Corr_Coeff: " << stats.corr_coeff;
    return out;
}


std::ostream& operator<<(std::ostream& out, std::vector<Statistics> stats) {
    for (auto &stat : stats) {
        out << stat << std::endl;
    }
    return out;
}

Region_f region_i2f(const Region& region) {
    Region_f region_f;
    for (auto& point : region) {
        region_f.push_back(point);
    }
    return region_f;

}
