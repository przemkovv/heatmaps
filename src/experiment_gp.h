
#pragma once

#include "types.h"

ExperimentSettings prepare_experiment_settings_parking(const std::string &hour) ;

ExperimentSettings prepare_experiment_settings_parking() ;

void experiment_GP(const ExperimentSettings &settings) ;
