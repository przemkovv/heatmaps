
#include "io.h"

#include <string.h>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/ml/ml.hpp>
#include "transformations.h"

void create_directory(const std::string  &path) {
    char tmp[256];
    char *p = NULL;
    size_t len;
    snprintf(tmp, sizeof(tmp),"%s", path.c_str());
    len = strlen(tmp);
    if(tmp[len - 1] == '/') tmp[len - 1] = 0;
    for(p = tmp + 1; *p; p++) 
        if(*p == '/') { 
            *p = 0;
            mkdir(tmp, S_IRWXU);
            *p = '/';
        }
    mkdir(tmp, S_IRWXU);
}

/* void create_directory(const std::string &path) { */ 

/*     struct stat st = {0}; */
/*     if (stat(path.c_str(), &st) == -1) */ 
/*     { */
/*         mkdir(path.c_str(), 0700); */ 
/*     } */
/* } */

template<>
void view(const std::string name, Heatmap& heatmap) {
    view(name, heatmap.image);
    view(name + "_mask", heatmap.mask);
}

Colormap load_colormap(const std::string& filename) {
    int dim = 256;

    auto colormap_image = cv::imread(filename);
    cv::Mat table (1, &dim, CV_8UC3);

    table.at<cv::Vec3b>(0)[0] = 0;
    table.at<cv::Vec3b>(0)[1] = 0;
    table.at<cv::Vec3b>(0)[2] = 0;

    for (int i = 1; i < dim;  ++i) {
        table.at<cv::Vec3b>(i) = colormap_image.at<cv::Vec3b>(i - 1, 0);
    }

    return table;

}

template<>
void save_image(const std::string name, Heatmap& heatmap, const std::string output_dir, const std::string filename_suffix) {
    save_image(name, heatmap.image, output_dir, filename_suffix);
}

Heatmap read_heatmap_from_csv(const std::string &name, bool normalization) {
    CvMLData ml_data;
    ml_data.read_csv(name.c_str());
    Heatmap heatmap;
    heatmap.image = cv::Mat(ml_data.get_values(), true);
    heatmap.image.convertTo(heatmap.image, CV_8UC1);
    heatmap.mask = get_mask(heatmap.image);

    if (normalization) {
        heatmap.image = normalize(heatmap.image, heatmap.mask);
        //cv::equalizeHist(heatmap.image, heatmap.image); // TODO: this function perform a bit different...
    }

    return heatmap;
}

cv::Mat load_image(const std::string& filename) {
    auto image = cv::imread(filename);

    if (image.empty()) {
        std::string message = filename + " image was not loaded!";
        std::cerr << message << std::endl;
        throw std::invalid_argument(message);
    }
    return image;

}

