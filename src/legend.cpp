
#include "legend.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include "transformations.h"

enum class TextPosition { Left, Right, Top, Bottom };
void drawText(const std::string& text, cv::Mat &image, TextPosition position) {

    const int margin = 5;

    int fontFace = cv::FONT_HERSHEY_SCRIPT_COMPLEX;
    double fontScale = 0.9;
    int thickness = 2;
    int baseline=0;
    cv::Size textSize =cv::getTextSize(text, fontFace, fontScale, thickness, &baseline);

    cv::Point textOrg;
    switch (position) {
        case TextPosition::Bottom:
            textOrg = { 
                (image.cols - textSize.width)/2,
                image.rows - textSize.height + margin
            };
            break;
        default:
        case TextPosition::Top:
            textOrg = { 
                (image.cols - textSize.width)/2,
                textSize.height + margin
            };
            break;
    }

    // then put the text itself
    cv::putText(image, text, textOrg, fontFace, fontScale,
            cv::Scalar::all(0), thickness, 8);
}

cv::Mat prepare_legend_vertical(cv::Mat& image, cv::Mat& colormap, int size, std::pair<int, int> range ) {
    auto height = image.rows;
    auto width = size;

    auto colormap_trimmed = colormap(cv::Rect(0, 1, 1, colormap.rows-1)); // remove black row

    cv::Mat  legend;
    cv::resize(colormap_trimmed, legend, {width, height});
    cv::flip(legend, legend, 0);
    drawText(std::to_string(range.first), legend, TextPosition::Bottom);
    drawText(std::to_string(range.second), legend, TextPosition::Top);
    return legend;
}

cv::Mat prepare_legend_horizontal(cv::Mat& image, cv::Mat& colormap, int size, std::pair<int, int> range ) {
    auto height = size;
    auto width = image.cols;

    auto colormap_trimmed = colormap(cv::Rect(0, 1, 1, colormap.rows-1)); // remove black row

    cv::Mat  legend;
    cv::resize(colormap_trimmed.t(), legend, {width, height});
    cv::flip(legend, legend, 0);
    drawText(std::to_string(range.first), legend, TextPosition::Left);
    drawText(std::to_string(range.second), legend, TextPosition::Right);
    return legend;
}


cv::Mat add_legend_at_left(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range ) {
    auto legend_vertical = prepare_legend_vertical(image, colormap, legend_size, range);
    return merge_horizontally(legend_vertical, image);
}

cv::Mat add_legend_at_right(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range) {
    auto legend_vertical = prepare_legend_vertical(image, colormap, legend_size, range);
    return merge_horizontally(image, legend_vertical);
}
cv::Mat add_legend_at_top(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range) {
    auto legend_horizontal = prepare_legend_horizontal(image, colormap, legend_size, range);
    return  merge_vertically(legend_horizontal, image);
}

cv::Mat add_legend_at_bottom(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range) {
    auto legend_horizontal = prepare_legend_horizontal(image, colormap, legend_size, range);
    return  merge_vertically(image, legend_horizontal);
}
