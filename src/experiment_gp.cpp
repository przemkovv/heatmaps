
#include "experiment_gp.h"

#include "transformations.h"
#include "io.h"
#include "legend.h"


extern const int legend_size;

ExperimentSettings prepare_experiment_settings_parking(const std::string &hour) {
    ExperimentSettings es;
    es.side_view_imagefile = "../data/GP/background_GP_" + hour + ".png";
    es.top_view_imagefile = "../data/GP/top_background.png";
    es.colormap_file = "../data/GP/colormap_log.png";

    es.heatmap_side_view_datafile = "../data/GP/densityMapCount_" + hour + "_norm.csv";

    es.input_data_normalization = false;
    es.statistics_computation = false;

    es.source_points = { 
        {1, 164},
        {720, 164},
        {720, 576},
        {1, 576}
    };

    es.dest_points = { 
        {739, 1},
        {739, 668},
        {1, 468},
        {1, 163}
    };

    es.output_dir = "experiment_GP";
    es.output_filename_suffix = "_" + hour;

    es.legend_range = std::make_pair(1, 188);
    return es;
}

ExperimentSettings prepare_experiment_settings_parking() {
    ExperimentSettings es;
    es.side_view_imagefile = "../data/GP/background_GP_06.png";
    es.top_view_imagefile = "../data/GP/top_background.png";
    es.colormap_file = "../data/GP/colormap_log.png";

    es.heatmap_side_view_datafile = "../data/GP/MAPA_dzienna.csv";

    es.input_data_normalization = false;
    es.statistics_computation = false;

    es.source_points = { 
        {1, 164},
        {720, 164},
        {720, 576},
        {1, 576}
    };

    es.dest_points = { 
        {739, 1},
        {739, 668},
        {1, 468},
        {1, 163}
    };

    es.output_dir = "experiment_GP";
    es.output_filename_suffix = "_daily";

    es.legend_range = std::make_pair(1, 871);
    return es;
}


void experiment_GP(const ExperimentSettings &settings) {


    const int sectors_x = 2;
    const int sectors_y = 2;

    std::string csv_output_filename = settings.output_dir + "/stats.csv";


    auto save = [&settings](auto filename, auto image) { save_image(filename, image, settings.output_dir, settings.output_filename_suffix); };

    auto colormap = load_colormap(settings.colormap_file);

    auto sv_image = load_image(settings.side_view_imagefile);
    auto tv_image = load_image(settings.top_view_imagefile);
    auto heatmap_sv = read_heatmap_from_csv(settings.heatmap_side_view_datafile, false);

    Region all_image = settings.dest_points;
    /* Region all_image { */
    /*     {tv_image.cols, tv_image.rows}, */
    /*     {0, tv_image.rows}, */
    /*     {0, 0}, */
    /*     {tv_image.cols, 0} */
    /* }; */
    
    auto legend_range_sv = settings.legend_range;

    auto heatmap_sv_as_tv = transform(settings.source_points, settings.dest_points, 
                                 heatmap_sv, tv_image.size());
    std::vector<Statistics> stats_sv_as_tv;

    save("side_view_heatmap", heatmap_sv.image);
    save("side_view_as_top_view_heatmap", heatmap_sv_as_tv.image);
    if (settings.input_data_normalization) {
        heatmap_sv.image = normalize(heatmap_sv.image, heatmap_sv.mask);
        heatmap_sv_as_tv.image = normalize(heatmap_sv_as_tv.image, heatmap_sv_as_tv.mask);
    }

    auto color_heatmap_sv = apply_colormap(heatmap_sv, colormap);
    auto color_heatmap_sv_as_tv = apply_colormap(heatmap_sv_as_tv, colormap);

    save("side_view_normed_heatmap", color_heatmap_sv.image);
    save("side_view_as_top_view_normed_heatmap", color_heatmap_sv_as_tv.image);

    auto blended_heatmap_sv = blend_images(sv_image, color_heatmap_sv); 
    blended_heatmap_sv = highlight_region(blended_heatmap_sv, settings.source_points);
    blended_heatmap_sv = add_legend_at_left(blended_heatmap_sv, colormap, legend_size, 
            legend_range_sv);

    view("sv", blended_heatmap_sv);
    save("side_view_blended", blended_heatmap_sv);

    auto blended_heatmap_sv_as_tv = blend_images(tv_image, color_heatmap_sv_as_tv); 
    blended_heatmap_sv_as_tv = highlight_region(blended_heatmap_sv_as_tv, settings.dest_points);
    blended_heatmap_sv_as_tv = add_legend_at_left(blended_heatmap_sv_as_tv, colormap, legend_size,
            legend_range_sv);

    view("sv_as_tv", blended_heatmap_sv_as_tv);
    save("side_view_as_top_view_blended", blended_heatmap_sv_as_tv);

}
