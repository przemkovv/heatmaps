
#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

struct Heatmap{
    cv::Mat image;
    cv::Mat mask;
};

typedef std::vector<cv::Point2i> Region;
typedef std::vector<cv::Point2f> Region_f;
typedef cv::Mat Colormap;
typedef std::pair<int, int> Range;

struct  ExperimentSettings {
    std::string side_view_imagefile;
    std::string top_view_imagefile;
    std::string colormap_file;
    std::string heatmap_side_view_datafile;
    std::string heatmap_top_view_datafile;

    Region source_points;
    Region dest_points;

    bool input_data_normalization;
    bool statistics_computation;

    std::string output_dir;
    std::string output_filename_suffix;
    Range legend_range ;

};

struct Statistics {
    double mean;
    double sum;
    double min;
    double max;
    double stddev;

    double corr_coeff;
    cv::Rect corr_coeff_location;

    int sector_id;
    
    Statistics() : mean(), sum(), min(), max(), stddev(), corr_coeff(), sector_id() { }
};


std::ostream& operator<<(std::ostream& out, const Heatmap& heatmap);
std::ostream& operator<<(std::ostream& out, Statistics stats); 
std::ostream& operator<<(std::ostream& out, std::vector<Statistics> stats); 

Region_f region_i2f(const Region& region);
