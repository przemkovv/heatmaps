
#include "types.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>


cv::Mat get_mask(cv::Mat& image, bool invert)
{
    cv::Mat mask;
    cv::threshold(image, mask, invert ? 255 : 0 , 255, invert ? cv::THRESH_BINARY_INV : cv::THRESH_BINARY);
    return mask;
}

std::pair<int, int> find_range(cv::Mat &matrix) {

   double min_sv, max_sv; 
   cv::minMaxLoc(matrix, &min_sv, &max_sv);
   return {min_sv+1, max_sv};
}

cv::Mat transform(Region src_points, Region dst_points, cv::Mat& image, cv::Size dst_size)
{
    auto transform = cv::findHomography(region_i2f(src_points), region_i2f(dst_points));
    cv::Mat transformed;
    cv::warpPerspective(image, transformed, transform, dst_size);
    return transformed;
}

Heatmap transform(Region src_points, Region dst_points, Heatmap& heatmap, cv::Size dst_size)
{
    Heatmap transformed;
    transformed.image = transform(src_points, dst_points, heatmap.image, dst_size);
    transformed.mask = transform(src_points, dst_points, heatmap.mask, dst_size);
    return transformed;
}



cv::Mat apply_colormap(cv::Mat image, cv::Mat& colormap)
{
    cv::cvtColor(image, image, CV_GRAY2BGR);
    image.convertTo(image, CV_8UC3);
    cv::Mat image_color;
    cv::LUT(image, colormap, image_color);
    return image_color;
}

Heatmap apply_colormap(Heatmap& heatmap, cv::Mat& colormap)
{
    return Heatmap {
        apply_colormap(heatmap.image, colormap),
        heatmap.mask.clone()
    };
}

Heatmap normalize(Heatmap& heatmap)
{

    double min, max;
    cv::minMaxLoc(heatmap.image, &min, &max, 0, 0, heatmap.mask);
    auto scale_factor = 255/max;
    std::cout << "Min: " << min << " Max: " << max <<std::endl;
    return { heatmap.image.clone() * scale_factor, heatmap.mask.clone() };
}

cv::Mat normalize(cv::Mat& image, cv::Mat& mask)
{
    double min, max;
    cv::minMaxLoc(image, &min, &max, 0, 0, mask);
    auto scale_factor = 255/max;
    std::cout << "Min: " << min << " Max: " << max <<std::endl;
    return image.clone() * scale_factor;
}



cv::Mat blend_images(cv::Mat& image1, cv::Mat& image2, cv::Mat& mask)
{
    auto inverted_mask = 255 - mask;
    cv::Mat image1_inv_mask;
    cv::bitwise_and(image1, image1, image1_inv_mask, inverted_mask);
    cv::Mat image1_mask;
    cv::bitwise_and(image1, image1, image1_mask, mask);
    cv::Mat image2_mask;
    cv::bitwise_and(image2, image2, image2_mask, mask);
    float alpha = 0.3;
    float beta = (1.0 - alpha);
    return alpha * image1_mask + beta * image2_mask + image1_inv_mask;
}

cv::Mat blend_images(cv::Mat& image1, Heatmap& heatmap)
{
    return blend_images(image1, heatmap.image, heatmap.mask);
}

cv::Mat merge_horizontally(cv::Mat& image1, cv::Mat& image2, int space = 1)
{
    assert(image1.rows == image2.rows);
    auto rows = image1.rows;
    auto cols = image1.cols + space + image2.cols;
    cv::Mat result = cv::Mat::zeros(rows, cols, image1.type());
    image1.copyTo(result(cv::Rect(0, 0, image1.cols, rows)));
    image2.copyTo(result(cv::Rect(image1.cols + space, 0, image2.cols, rows)));
    return result;
}

cv::Mat merge_vertically(cv::Mat& image1, cv::Mat& image2, int space = 1)
{
    assert(image1.cols == image2.cols);
    auto rows = image1.rows + space + image2.rows;;
    auto cols = image1.cols;
    cv::Mat result = cv::Mat::zeros(rows, cols, image1.type());
    image1.copyTo(result(cv::Rect(0, 0, cols, image1.rows)));
    image2.copyTo(result(cv::Rect(0, image1.rows + space, cols, image2.rows)));
    return result;
}

cv::Mat highlight_region(cv::Mat& image, const Region& region)
{
    auto result = image.clone();
    auto color = CV_RGB(255, 0, 0);
    std::vector<Region> regions { region };
    cv::polylines(result, regions, true, color, 5, CV_AA, 0);
    //cv::fillConvexPoly(result, region, region.size(), color, 8, 0);
    return result;
}


cv::Mat highlight_region(cv::Mat& image, const cv::Rect& region)
{
    auto result = image.clone();
    auto color = CV_RGB(0, 0, 255);
    std::vector<cv::Rect> regions { region };
    cv::rectangle(result, region.tl(), region.br(), color, 2, CV_AA, 0);
    /* cv::polylines(result, regions, true, color, 5, CV_AA, 0); */
    //cv::fillConvexPoly(result, region, region.size(), color, 8, 0);
    return result;
}

