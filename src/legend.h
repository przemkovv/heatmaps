#pragma once

#include <utility>
#include "types.h"

cv::Mat prepare_legend_vertical(cv::Mat& image, cv::Mat& colormap, int size, std::pair<int, int> range ) ;
cv::Mat prepare_legend_horizontal(cv::Mat& image, cv::Mat& colormap, int size, std::pair<int, int> range ) ;

cv::Mat add_legend_at_left(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range);
cv::Mat add_legend_at_right(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range);
cv::Mat add_legend_at_top(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range);
cv::Mat add_legend_at_bottom(cv::Mat& image, cv::Mat& colormap, int legend_size, std::pair<int, int> range);
