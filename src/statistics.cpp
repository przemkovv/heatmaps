#include "statistics.h"

#include <fstream>

#include <boost/range/irange.hpp>


Statistics compute_statistics(cv::Mat matrix, cv::InputArray mask) {
    Statistics stats;

    cv::minMaxLoc(matrix, &stats.min, &stats.max, 0, 0, mask);
    cv::Mat mean, stddev;
    cv::meanStdDev(matrix, mean, stddev, mask);
    stats.mean = mean.at<double>(0);
    stats.stddev = stddev.at<double>( 0 );
    stats.sum = cv::sum(matrix)[0];
    stats.corr_coeff = 0;
    return stats;

}

std::vector<cv::Rect> get_sectors(const Region &region, int sectors_x, int sectors_y) {

    cv::Point top_left = region[2];
    cv::Point bottom_right = region[0];

    auto size = cv::Size(bottom_right - top_left);
    size.width /= sectors_x;
    size.height /= sectors_y;

    std::vector<cv::Rect> sectors;

    for (int sx = 0; sx < sectors_x; ++sx) {
        for (int sy = 0; sy < sectors_y; ++sy) {
            sectors.push_back(cv::Rect{cv::Point{top_left.x + size.width * sx, top_left.y + size.height * sy},
                                        size});
        }
    }
    return sectors;
}

std::vector<Statistics> compute_correlation(cv::Mat &matrix1, cv::Mat &matrix2, const Region &region, int sectors_x, int sectors_y) {

    auto sectors = get_sectors(region, sectors_x, sectors_y);

    std::vector<cv::Mat> corrs;
    corrs.reserve(sectors.size());

    std::transform(sectors.begin(), sectors.end(), std::back_inserter(corrs), 
            [&matrix1,&matrix2](auto sector) { 
            cv::Mat result;
            cv::matchTemplate(
                    /* cv::Mat{matrix1, sector}, */ 
                    matrix1,
                    cv::Mat{matrix2, sector}, 
                    /* result, CV_TM_CCOEFF_NORMED); */ 
                    result, CV_TM_CCORR_NORMED); 
            return result;
            });

    double min, max;
    cv::Point minLoc, maxLoc;
    std::vector<Statistics> stats{sectors.size()};

    for (auto i : boost::irange(0ul, sectors.size())) {
        cv::minMaxLoc(corrs[i], &min, &max, &minLoc, &maxLoc);
        stats[i].corr_coeff = max;
        /* auto r = cv::Rect{cv::Point{ maxLoc.x, maxLoc.y}, sectors[i].size()}; */
        auto r = cv::Rect{cv::Point{ maxLoc.x - sectors[i].x, maxLoc.y - sectors[i].y}, sectors[i].size()};
        stats[i].corr_coeff_location = r;
        stats[i].sector_id = i;
    }
        
    return stats;
    
}

std::vector<Statistics> compute_local_statistics(Heatmap &heatmap, const Region &region, int sectors_x, int sectors_y, bool use_mask) {

    auto sectors = get_sectors(region, sectors_x, sectors_y);

    std::vector<Statistics> stats;
    stats.reserve(sectors_x * sectors_y);

    std::transform(sectors.begin(), sectors.end(), std::back_inserter(stats), 
            [&heatmap,use_mask](auto sector) { return compute_statistics(
                cv::Mat{heatmap.image, sector},
                use_mask ? cv::Mat{heatmap.mask, sector} : cv::noArray()); });

    return stats;
    
}

Statistics compute_statistics_errors(const Statistics &stats1, const Statistics &stats2) {
    Statistics errors;

    auto error = [](auto x1, auto x2) { return (x1-x2)/x1 * 100; };

    errors.mean = error(stats1.mean, stats2.mean);
    errors.max = error(stats1.max, stats2.max);
    errors.min = error(stats1.min, stats2.min);
    errors.stddev = error(stats1.stddev, stats2.stddev);
    errors.sum = error(stats1.sum, stats2.sum);
    errors.corr_coeff = 0;

    return errors;

}

std::vector<Statistics> compute_statistics_errors(std::vector<Statistics> stats1, std::vector<Statistics> stats2) {
    
    std::vector<Statistics> errors;
    std::transform(stats1.begin(), stats1.end(), stats2.begin(), std::back_inserter(errors), 
            [](auto s1, auto s2) { return compute_statistics_errors(s1, s2); });
    return errors;
}

std::string get_csv_title_row_from_statistics(std::string title) {
    std::stringstream csv_record;
    const auto separator = ",";

    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << title << std::endl;
    return csv_record.str();
}

std::string get_csv_empty_row_from_statistics() {
    std::stringstream csv_record;
    const auto separator = ",";

    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << separator;
    csv_record << std::endl;
    return csv_record.str();
}
std::string get_csv_header_from_statistics() {
    std::stringstream csv_record;
    const auto separator = ",";
    csv_record << "ID" << separator;
    csv_record << "Sum" << separator;
    csv_record << "Min" << separator;
    csv_record << "Max" << separator;
    csv_record << "Mean" << separator;
    csv_record << "Stddev" << separator;
    csv_record << "Corr_Coeff" << separator;
    csv_record << "Corr_Coeff_x" << separator;
    csv_record << "Corr_Coeff_y" << separator ;
    csv_record << "Corr_Coeff_w" << separator;
    csv_record << "Corr_Coeff_h" << separator << std::endl;
    return csv_record.str();
}

std::string get_csv_record_from_statistics(Statistics stat) {
    std::stringstream csv_record;
    const auto separator = ",";
    csv_record << stat.sector_id  << separator;
    csv_record << stat.sum  << separator;
    csv_record << stat.min  << separator;
    csv_record << stat.max  << separator;
    csv_record << stat.mean  << separator;
    csv_record << stat.stddev  << separator;
    csv_record << stat.corr_coeff  << separator;
    csv_record << stat.corr_coeff_location.x  << separator;
    csv_record << stat.corr_coeff_location.y  << separator ;
    csv_record << stat.corr_coeff_location.width  << separator;
    csv_record << stat.corr_coeff_location.height  << separator << std::endl;;
    return csv_record.str();
}

void save_statistics_to_csv(std::vector<Statistics> stats, std::fstream &output_file) {
    for (auto &stat : stats) {
        output_file << get_csv_record_from_statistics(stat);
    }
}

void save_statistics_to_csv(std::vector<Statistics> stats1, std::vector<Statistics> stats2,
        std::vector<Statistics> errors, 
        std::vector<Statistics> sector_corr, 
        std::vector<Statistics> overall_corr, 
        std::string output_filename) {
    std::fstream csv_file{ output_filename.c_str(), std::ios::out | std::ios::trunc };
    csv_file << get_csv_header_from_statistics();

    csv_file << get_csv_title_row_from_statistics("Top view");

    save_statistics_to_csv(stats1, csv_file);
    csv_file <<get_csv_empty_row_from_statistics();
    csv_file << get_csv_title_row_from_statistics("Side view");
    save_statistics_to_csv(stats2, csv_file);
    csv_file <<get_csv_empty_row_from_statistics();
    csv_file << get_csv_title_row_from_statistics("Errors");
    save_statistics_to_csv(errors, csv_file);
    csv_file <<get_csv_empty_row_from_statistics();
    csv_file << get_csv_title_row_from_statistics("Sectors correlation");
    save_statistics_to_csv(sector_corr, csv_file);
    csv_file <<get_csv_empty_row_from_statistics();
    csv_file << get_csv_title_row_from_statistics("Overall correlation");
    save_statistics_to_csv(overall_corr, csv_file);
}
