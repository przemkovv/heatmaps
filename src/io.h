
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <opencv2/highgui/highgui.hpp>

#include "types.h"

void create_directory(const std::string &path);

template<typename M>
void view(const std::string name, M&& image) {
    std::cout << name << std::endl;
    cv::namedWindow(name);
    cv::imshow(name, image);
    cv::waitKey();
}

template<> 
void view(const std::string name, Heatmap& heatmap);

Colormap load_colormap(const std::string& filename);
template<typename M>
void save_image(const std::string name, M&& image, const std::string output_dir, const std::string filename_suffix = "") {
    create_directory(output_dir);
    auto filename = output_dir + "/" + name + filename_suffix + ".png";
    cv::imwrite(filename, image);
}

template<>
void save_image(const std::string name, Heatmap& heatmap, const std::string output_dir, const std::string filename_suffix);


Heatmap read_heatmap_from_csv(const std::string &name, bool normalization = false);
cv::Mat load_image(const std::string& filename);

