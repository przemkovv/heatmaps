#pragma once

#include "types.h"


Statistics compute_statistics(cv::Mat matrix, cv::InputArray mask = cv::noArray()) ;

std::vector<cv::Rect> get_sectors(const Region &region, int sectors_x, int sectors_y) ;

std::vector<Statistics> compute_correlation(cv::Mat &matrix1, cv::Mat &matrix2, const Region &region, int sectors_x, int sectors_y) ;

std::vector<Statistics> compute_local_statistics(Heatmap &matrix, const Region &region, int sectors_x, int sectors_y, bool use_mask = false) ;

Statistics compute_statistics_errors(const Statistics &stats1, const Statistics &stats2) ;

std::vector<Statistics> compute_statistics_errors(std::vector<Statistics> stats1, std::vector<Statistics> stats2) ;

std::string get_csv_title_row_from_statistics(std::string title) ;

std::string get_csv_empty_row_from_statistics() ;
std::string get_csv_header_from_statistics() ;

std::string get_csv_record_from_statistics(Statistics stat) ;

void save_statistics_to_csv(std::vector<Statistics> stats, std::fstream &output_file) ;

void save_statistics_to_csv(std::vector<Statistics> stats1, std::vector<Statistics> stats2,
        std::vector<Statistics> errors, 
        std::vector<Statistics> sector_corr, 
        std::vector<Statistics> overall_corr, 
        std::string output_filename) ;
