#include "experiment_bugs.h"


#include "transformations.h"
#include "io.h"
#include "legend.h"
#include "statistics.h"

extern const int legend_size;


ExperimentSettings prepare_experiment_settings_30st()
{
    ExperimentSettings es;
    es.side_view_imagefile = "../data/30st/side_view_background.png";
    es.top_view_imagefile = "../data/30st/top_view_background.png";
    es.colormap_file = "../data/30st/colormap_lin.png";
    es.heatmap_side_view_datafile = "../data/30st/side_view.csv";
    es.heatmap_top_view_datafile = "../data/30st/top_view.csv";
    es.input_data_normalization = false;
    es.statistics_computation = true;
    es.source_points = {
        {121, 37},
        {460, 31},
        {501, 362},
        {51, 360}
    };
    es.dest_points = {
        {507, 457},
        {106, 462},
        {108, 62},
        {495, 61}
    };
    es.output_dir = "experiment2/30st";
    return es;
}
ExperimentSettings prepare_experiment_settings_35st()
{
    ExperimentSettings es;
    es.side_view_imagefile = "../data/35st/side_view_background.png";
    es.top_view_imagefile = "../data/35st/top_view_background.png";
    es.colormap_file = "../data/35st/colormap_lin.png";
    es.heatmap_side_view_datafile = "../data/35st/side_view.csv";
    es.heatmap_top_view_datafile = "../data/35st/top_view.csv";
    es.input_data_normalization = false;
    es.statistics_computation = true;
    es.source_points = {
        {150, 41},
        {498, 35},
        {561, 361},
        {68, 357}
    };
    es.dest_points = {
        {510, 460},
        {105, 461},
        {108, 61},
        {496, 61}
    };
    es.output_dir = "experiment2/35st";
    return es;
}


ExperimentSettings prepare_experiment_settings_40st()
{
    ExperimentSettings es;
    es.side_view_imagefile = "../data/40st/side_view_background.png";
    es.top_view_imagefile = "../data/40st/top_view_background.png";
    es.colormap_file = "../data/40st/colormap_lin.png";
    es.heatmap_side_view_datafile = "../data/40st/side_view.csv";
    es.heatmap_top_view_datafile = "../data/40st/top_view.csv";
    es.input_data_normalization = false;
    es.statistics_computation = true;
    es.source_points = {
        {133, 17},
        {520, 18},
        {623, 370},
        {3, 355}
    };
    es.dest_points = {
        {508, 459},
        {103, 460},
        {109, 60},
        {497, 63}
    };
    es.output_dir = "experiment2/40st";
    return es;
}
ExperimentSettings prepare_experiment_settings_45st()
{
    ExperimentSettings es;
    es.side_view_imagefile = "../data/45st/side_view_background.png";
    es.top_view_imagefile = "../data/45st/top_view_background.png";
    es.colormap_file = "../data/45st/colormap_lin.png";
    es.heatmap_side_view_datafile = "../data/45st/side_view.csv";
    es.heatmap_top_view_datafile = "../data/45st/top_view.csv";
    es.input_data_normalization = false;
    es.statistics_computation = true;
    es.source_points = {
        {150, 49},
        {506, 48},
        {611, 343},
        {40, 339}
    };
    es.dest_points = {
        {508, 460},
        {104, 461},
        {110, 61},
        {497, 63}
    };
    es.output_dir = "experiment2/45st";
    return es;
}

ExperimentSettings prepare_experiment_settings_50st()
{
    ExperimentSettings es;
    es.side_view_imagefile = "../data/50st/side_view_background.png";
    es.top_view_imagefile = "../data/50st/top_view_background.png";
    es.colormap_file = "../data/50st/colormap_lin.png";
    es.heatmap_side_view_datafile = "../data/50st/side_view.csv";
    es.heatmap_top_view_datafile = "../data/50st/top_view.csv";
    es.input_data_normalization = false;
    es.statistics_computation = true;
    es.source_points = {
        {123, 44},
        {504, 41},
        {650, 334},
        {-15, 340}
    };
    es.dest_points = {
        {509, 459},
        {105, 461},
        {109, 62},
        {496, 61}
    };
    es.output_dir = "experiment2/50st";
    return es;
}

void save(std::string filename, cv::Mat image, ExperimentSettings settings)
{
    save_image(filename, image, settings.output_dir, settings.output_filename_suffix);
};

cv::Mat generate_final_image(std::string name, cv::Mat image, Heatmap& heatmap, const Region& region, Colormap colormap, const Range& legend_range, ExperimentSettings settings)
{
    auto color_heatmap = apply_colormap(heatmap, colormap);
    auto blended_heatmap = blend_images(image, color_heatmap);
    blended_heatmap = highlight_region(blended_heatmap, region);
    blended_heatmap = add_legend_at_left(blended_heatmap, colormap, legend_size,
                                         legend_range);
    view(name, blended_heatmap);
    save(name, blended_heatmap, settings);

    return blended_heatmap;
}

Range get_correct_range(Heatmap& heatmap, const Range& range) {
    if (range.first == 0 && range.second == 0) {
        return find_range(heatmap.image);
    }
    return range;
}

void experiment_bugs(const ExperimentSettings& settings)
{
    const int sectors_x = 2;
    const int sectors_y = 8;
    std::string csv_output_filename = settings.output_dir + "/stats.csv";
    std::string csv_output_mask_filename = settings.output_dir + "/stats_mask.csv";

    auto save = [&settings](auto filename, auto image) {
        save_image(filename, image, settings.output_dir, settings.output_filename_suffix);
    };
    auto colormap = load_colormap(settings.colormap_file);
    auto sv_image = load_image(settings.side_view_imagefile);
    auto tv_image = load_image(settings.top_view_imagefile);
    auto heatmap_sv = read_heatmap_from_csv(settings.heatmap_side_view_datafile, false);
    auto heatmap_tv = read_heatmap_from_csv(settings.heatmap_top_view_datafile, false);

    Region region = settings.dest_points;
    auto legend_range_sv = get_correct_range(heatmap_sv, settings.legend_range);
    auto legend_range_tv = get_correct_range(heatmap_tv, settings.legend_range);

    auto heatmap_sv_as_tv = transform(settings.source_points, settings.dest_points,
                                      heatmap_sv, tv_image.size());

    save("side_view_heatmap", heatmap_sv.image);
    save("side_view_as_top_view_heatmap", heatmap_sv_as_tv.image);
    save("top_view_heatmap", heatmap_tv.image);

    /* if (settings.input_data_normalization) { */
    auto heatmap_sv_norm = normalize(heatmap_sv);
    auto heatmap_sv_as_tv_norm = normalize(heatmap_sv_as_tv);
    auto heatmap_tv_norm = normalize(heatmap_tv);
    /* } */

    generate_final_image("side_view_blended", sv_image, heatmap_sv_norm, 
            settings.source_points, colormap, legend_range_sv, settings);
    auto sv_as_tv_blended = generate_final_image("side_view_as_top_view_blended", 
            tv_image, heatmap_sv_as_tv_norm,
            settings.dest_points, colormap, legend_range_sv, settings);
    generate_final_image("top_view_blended", tv_image, heatmap_tv_norm,
            settings.dest_points, colormap, legend_range_tv, settings);

    if (settings.statistics_computation) {
        std::vector<Statistics> stats_sv_as_tv;
        std::vector<Statistics> stats_tv;


        stats_sv_as_tv = compute_local_statistics(heatmap_sv_as_tv, region, sectors_x, sectors_y);
        std::cout << stats_sv_as_tv << std::endl;

        stats_tv = compute_local_statistics(heatmap_tv, region, sectors_x, sectors_y);
        std::cout << stats_tv << std::endl;

        auto errors = compute_statistics_errors(stats_tv, stats_sv_as_tv);
        std::cout << "Errors: " << std::endl;
        std::cout << errors << std::endl;

        auto sectors_corr = compute_correlation(heatmap_tv.image, heatmap_sv_as_tv.image, region, sectors_x, sectors_y);
        std::cout << "Corr: " << std::endl;
        std::cout << sectors_corr << std::endl;

        auto image_corr = compute_correlation(heatmap_tv.image, heatmap_sv_as_tv.image, region, 1, 1);
        std::cout << "Corr: " << std::endl;
        std::cout << image_corr << std::endl;

        save_statistics_to_csv(stats_tv, stats_sv_as_tv, errors, sectors_corr, image_corr, csv_output_filename);

        cv::Mat sum_mask;
        cv::bitwise_or(heatmap_sv_as_tv.mask, heatmap_tv.mask, sum_mask);
        
        stats_sv_as_tv = compute_local_statistics(heatmap_sv_as_tv, region, sectors_x, sectors_y, true);
        std::cout << stats_sv_as_tv << std::endl;

        stats_tv = compute_local_statistics(heatmap_tv, region, sectors_x, sectors_y, true);
        std::cout << stats_tv << std::endl;

        errors = compute_statistics_errors(stats_tv, stats_sv_as_tv);
        std::cout << "Errors: " << std::endl;
        std::cout << errors << std::endl;

        sectors_corr = compute_correlation(heatmap_tv.image, heatmap_sv_as_tv.image, region, sectors_x, sectors_y);
        std::cout << "Corr: " << std::endl;
        std::cout << sectors_corr << std::endl;

        image_corr = compute_correlation(heatmap_tv.image, heatmap_sv_as_tv.image, region, 1, 1);
        std::cout << "Corr: " << std::endl;
        std::cout << image_corr << std::endl;

        save_statistics_to_csv(stats_tv, stats_sv_as_tv, errors, sectors_corr, image_corr, csv_output_mask_filename);
        cv::Mat hh = sv_as_tv_blended.clone();
        for (auto sector : sectors_corr) {
            sector.corr_coeff_location.x += legend_size;
            hh = highlight_region(hh, sector.corr_coeff_location);
        }


        view("hh", hh);
        save("sv_as_tv_max_corr", hh);



    }
}
