\documentclass[conference]{IEEEtran}
\usepackage{cite}
\usepackage[cmex10]{amsmath}
\interdisplaylinepenalty=2500
\usepackage{array}
\usepackage[caption=true,font=footnotesize]{subfig}
\usepackage{url}
\usepackage[nolist]{acronym}
\usepackage{todonotes}

%do every todo note inline
\let\todox\todo
\renewcommand\todo[1]{\todox[inline]{#1}}

\usepackage{listings}
\usepackage[all]{nowidow}
% *** Do not adjust lengths that control margins, column widths, etc. ***
% *** Do not use packages that alter fonts (such as pslatex).         ***
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{MnSymbol}
% For importing .pdf_tex files that aren't in document's dir
\usepackage{import}
\usetikzlibrary{positioning, backgrounds, shapes}


\renewcommand{\vec}[1]{\mathbf{#1}}



\title{Projective  transformation}
\captionsetup{justification=centering}


\author{}
\begin{document}
\maketitle

\begin{acronym}
    \acro{AC}{Example Acronym}
\end{acronym}

\begin{abstract} 
 

\end{abstract}

\section{Homography}

During experiment EX1, the camera was positioned at the angle $60^\circ$
looking at the parking (see Figure~\ref{fig:side_view}). However, the data have
to be transformed to the view from top perspective (see
Figure~\ref{fig:top_view}) which is preferred by security and space planning
companies. Therefore, a homography (a projective transformation) has to be found and applied.

\subsection{Definition}
The projective transformation is a mapping between two planes, e.g. between
image and world plane. In our case we had to find a mapping between two images
of the same scene (from top and side view). It can be realized by determination
of two projective transformation from one image to world plane and from world
plane to second image (see Figure~\ref{fig:projection}).
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/projection.png}
    \caption{The projective transformation between two images induced by world
    plane.}
    \label{fig:projection}
\end{figure}


The homography is defined as a following transformation\cite{Hartley2004}:
\begin{equation}
    \label{eq:homography}
    \vec{x}^\prime = H\vec{x},
\end{equation}
where $H$ is a block matrix of form
\begin{equation}
    H = \left[ \begin{array}{cc}
            A & \vec{t} \\
            \vec{v}^T & v 
    \end{array} \right],
\end{equation}
where linear component $A$, in form of $2\times 2$ non-singular matrix,
represents an affine transformation (rotation and scaling) and vector $\vec{t}$
represents a translation and vector $\vec{v} = (v_1, v_2)^T$. The $v$ variable is only
for matrix scaling purpose. 

The chain of multiple transformation can be composed into one.
\begin{align}
    \vec{x}^\prime &= H_1\vec{x} \\
    \vec{x}^{\prime\prime} &= H_2\vec{x}^\prime \\
    \vec{x}^{\prime\prime} &= H_2 H_1 \vec{x} = H_3 \vec{x}
\end{align}

Therefore, only one transformation matrix have to be computed to map between
two images of the same scene.

\subsection{Estimation}

A projective transformation matrix can be computed using Direct Linear
Transformation (DLT) algorithm \cite{Hartley2004} which solves
Equation~\ref{eq:homography} using a given set of four point correspondences.
However, the algorithm requires that no three points are collinear on either
plane.



\section{Experiments}

\subsection{Heatmap projective transformation}

One of the steps of the experiment is to acquire data from the CCTV system and
then process them to present traffic load. The acquired data were processed by
algorithm X which resulted in a heatmap~(see
Figure~\ref{fig:side_view_heatmap}) representing traffic in each region of
scene over time.

The next step is to transform a heatmap to showing the data from top view.
However, to perform this step a homography matrix had to be computed first.

Therefore, a set of four corresponding points were selected for each view of
scene. The visualization of them is showed in Figures~\ref{fig:side_view_frame}
and~\ref{fig:top_view_frame}. The computations were performed by the opencv
library~\cite{opencv_library}. The results can be seen in
Figures~\ref{fig:top_view_heatmap} and~\ref{fig:top_view_heatmap_legend}.



\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/side_view}
    \caption{Side view of the scene.}
    \label{fig:side_view}
\end{figure}


\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/side_view_region}
    \caption{Side view of the scene with marked projective frame.}
    \label{fig:side_view_frame}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/top_view}
    \caption{Top view of the scene.}
    \label{fig:top_view}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/top_view_region}
    \caption{Top view of the scene with marked projective frame.}
    \label{fig:top_view_frame}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/side_view_heatmap}
    \caption{Heatmap from side perspective.}
    \label{fig:side_view_heatmap}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/top_view_heatmap}
    \caption{Heatmap from top perspective.}
    \label{fig:top_view_heatmap}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/top_view_heatmap_legend}
    \caption{Heatmap from top perspective blended with the scene image.}
    \label{fig:top_view_heatmap_legend}
\end{figure}


\subsection{Projective transformation evaluation}

The goal of the experiment is to show how accurate is projective transformation
applied to the heatmap, therefore, we prepared a testing environment with to
cameras looking at the scene from two different angles (see
Figure~\ref{fig:scene_diagram}). The first camera was positioned in straight
down direction, the angle of second camera was modified across multiple
experiments from 30 degree to 50 degree.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/scene_diagram.png}
    \caption{Cameras' positioning.}
    \label{fig:scene_diagram}
\end{figure}


A result of the experiment is data of the same scene and movement recorded from
two different views.  

In the next step, we  processed the acquired data using Algorithm X and we
generated heat maps (see
Figures~\ref{fig:side_view_50st}~and~\ref{fig:top_view_50st}). Finally, the
projective transformation of the marked regions was performed resulting in
Figure~\ref{fig:side_view_as_top_view_50st}.

An accuracy of the projective transformation of heatmap was measured and
compared using correlation as similarity measure between transformed heatmap
recorded from side view and non-transformed heatmap recored from top view.  At
first, we computed correlation coefficient between whole marked regions. The
results are in Table~1. An overall value of the measure is drops from $0.792$
to $0.671$ across ascending angle of the side camera.  In the next, we divided
each heatmap into four equal sectors and compared them respectively (see
Table~1). 

Additionally, to show a distortion we computed cross correlation between
sectors and the whole region and we found the best fitting regions. As we can
see in Figure~\ref{fig:side_top_viwe_corr_50st} the best fitting sectors are
shifted into the top center direction. The values of the correlation
coefficients of the best fitted sectors and whole region can be found in
Table~2 and are higher than in the direct comparison (Table~1). 

The shift of the best fitted sectors is related to the method of identifying
objects by Algorithm X. Both heatmaps (side and top view) were generated using
the center of the object found in the scene. However, the location of the
found center of the same object is different in the image made from side and
from top (for this view is very close to the real center of the object).
To prevent this shift, and to increase accuracy of the projective
transformation, a modification of the Algorithm X is needed to correctly
recognize a center of objects.




\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/side_view_50st.png}
    \caption{Side view from camera positioned at angle 50degrees.}
    \label{fig:side_view_50st}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/top_view_50st.png}
    \caption{Top view with the second camera positioned at angle 50degrees.}
    \label{fig:top_view_50st}
\end{figure}
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/side_view_as_top_view_50st.png}
    \caption{Side view transformed to the top view}
    \label{fig:side_view_as_top_view_50st}
\end{figure}
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{img/side_top_view_corr_50st.png}
    \caption{Offset of the sectors with highest correlation}
    \label{fig:side_top_viwe_corr_50st}
\end{figure}
    
\bibliographystyle{IEEEtran}
\bibliography{references}

\end{document}
